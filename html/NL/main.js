var style_object_wijken = {
  "version": 8,
  "name": "cbswijk",
  "sources": {
    "wijken":{
      "type": "vector",
      "tiles": [
    	"http://127.0.0.1:8080/maps/cbs/{z}/{x}/{y}.vector.pbf?debug=true"
      ]
    },
  },

  "layers":[
    {
      "id":  "background",
      "type": "background",
      "paint": {
        "background-color":"#FFFFFF"
      }
    },

    {
      "id": "cbswijk",
      "type": "fill-extrusion",
      "source": "wijken",
      "source-layer": "wijken",
      "maxzoom": 21,
      "minzoom": 0,
      "paint": {
        "fill-extrusion-base": 0,
        "fill-extrusion-height": {
        	"property": 'aantal_huishoudens',
			"stops": [
        		[{zoom: 12, value: 1}, 1],
        		[{zoom: 12, value: 3500}, 500],
        		[{zoom: 14, value: 1}, 1],
        		[{zoom: 14, value: 1500}, 500]
			]
        },

        "fill-extrusion-color": {
        	"property": 'aantal_huishoudens',
        	"stops": [
        		//[1,   "rgb(0,141,81)"],
        		[0,   "rgb(1,21, 41)"],
        		[230,  "rgb(1,41, 81)"],
        		[450, "rgb(1,19,157)"],
        		[700, "rgb(4,37,212)"],
        		[1055, "rgb(6,74,243)"],
        		[1584, "rgb(13,149,233)"],
        		[2256, "rgb(25,207,210)"],
        		[3098, "rgb(130,253,206)"],
        		[4235, "rgb(104,222,134)"],
        		[6200, "rgb(164,235,79)"],
        		[8000, "rgb(255,255,128)"],
        		[12000, "rgb(255,214,0)"],
        		[20000, "rgb(254,173,91)"],
        		[25000, "rgb(250,124,1)"],
        		[30000, "rgb(254,0,0)"],
        		[35000, "rgb(129,0,65)"],
        		[37000, "rgb(65,0,64)"],
        		[40000, "rgb(35,0,34)"]
        	]
        },

        "fill-extrusion-opacity": {
        	"base": 1,
        	"stops": [[12,1],[12.5,0.9],[14,0.82]]
    	}
      }
    },
  ]
};

//Initialize Map
var map = new mapboxgl.Map({
    container: "map",
    hash: true,
    style: style_object_wijken,
    zoom: 14,
    pitch: 60,
    bearing: 60,
    center: [4.88, 52.35]
});
