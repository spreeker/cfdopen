#!/usr/bin/env bash

set -u   # crash on missing env variables
set -e   # stop on any error
set -x   # print what we are doing

DIR=`dirname "$0"`

dc() {
	docker-compose -p cdf_${ENVIRONMENT} -f ${DIR}/docker-compose.yml $*
}


ENV="acceptance"
if [ "$ENVIRONMENT" == "production" ]; then
  ENV="production"
fi

# cleanup after exit
trap 'dc down -v; dc rm -f' EXIT

echo "Building / pull / cleanup images"
dc down
dc rm -f
dc pull
dc build

echo "Bringing up and waiting for database"
dc up -d database
# dc run importer /app/deploy/docker-wait.sh

# start ETL in docker / airflow. 

