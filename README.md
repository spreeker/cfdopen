
Common Data Factory
===================

A poc containing dutch datasets.

 - nlextract BAG
 - CBS neighborhoods
 - open energie data

sample etl proces loading data into postgres

run
===

     $docker-compose pull
     $docker-compose build
     $docker-compose up

maps
====

 - neighborhood with statistical information
 - neighborhood with energie information

example commands:

    $ docker-compose exec gdal bash
    $ ogr2ogr -overwrite -f "PostgreSQL" PG:"host=database user=cdf dbname=cdf password=insecure" cbs_wijken_2018_2019-10-21.gml

Load cbs wijken 2018 directly into database

    $ docker-compose run gdal ogr2ogr -overwrite -f "PostgreSQL" PG:"host=database user=cdf dbname=cdf password=insecure" -nln cbs_wijken_2018 'https://geodata.nationaalgeoregister.nl/wijkenbuurten2018/wfs?REQUEST=GetFeature&TYPENAME=cbs_wijken_2018&SERVICE=WFS&VERSION=2.0.0'


