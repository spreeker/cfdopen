#!/usr/bin/env bash

set -u   # crash on missing env variables
set -e   # stop on any error
set -x   # print what we are doing


# importeer buurt/wijk
python load_wfs_postgres.py https://geodata.nationaalgeoregister.nl/wijkenbuurten2018/wfs cbs_wijken_2018 28992

